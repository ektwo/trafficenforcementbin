#!/usr/bin/env python

#############################################################################
##
## Copyright (C) 2010 Hans-Peter Jansen <hpj@urpla.net>.
## Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
## All rights reserved.
##
## This file is part of the examples of PyQt.
##
## $QT_BEGIN_LICENSE:LGPL$
## Commercial Usage
## Licensees holding valid Qt Commercial licenses may use this file in
## accordance with the Qt Commercial License Agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Nokia.
##
## GNU Lesser General Public License Usage
## Alternatively, this file may be used under the terms of the GNU Lesser
## General Public License version 2.1 as published by the Free Software
## Foundation and appearing in the file LICENSE.LGPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU Lesser General Public License version 2.1 requirements
## will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
##
## In addition, as a special exception, Nokia gives you certain additional
## rights.  These rights are described in the Nokia Qt LGPL Exception
## version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## If you have questions regarding the use of this file, please contact
## Nokia at qt-info@nokia.com.
## $QT_END_LICENSE$
##
#############################################################################


# This is only needed for Python v2 but is harmless for Python v3.
#import sip
#sip.setapi('QString', 2)

#from PyQt5 import QtCore, QtGui, QtWidgets
#
#from dialog import Ui_Dialog
import struct
import array, sip
import PyQt5.QtCore as PQ
from PyQt5.QtCore import QBuffer, QDataStream, QSharedMemory, QIODevice, QByteArray, QRect
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QApplication, QDialog, QFileDialog

from dialog import Ui_Dialog
import cv2
SEND_RAW_FILE=1
ROLE_IS_MASTER=0
#MASTER_IS_TR=1
import numpy as np

class Dialog(QDialog):
#class Dialog(QtWidgets.QDialog):
    """ This class is a simple example of how to use QSharedMemory.  It is a
    simple dialog that presents a few buttons.  Run the executable twice to
    create two processes running the dialog.  In one of the processes, press
    the button to load an image into a shared memory segment, and then select
    an image file to load.  Once the first process has loaded and displayed the
    image, in the second process, press the button to read the same image from
    shared memory.  The second process displays the same image loaded from its
    new location in shared memory.
    The class contains a data member sharedMemory, which is initialized with
    the key "QSharedMemoryExample" to force all instances of Dialog to access
    the same shared memory segment.  The constructor also connects the
    clicked() signal from each of the three dialog buttons to the slot function
    appropriate for handling each button.
    """

    def __init__(self, parent = None):
        super(Dialog, self).__init__(parent)

        self.sharedMemory = QSharedMemory('sharedmemory_dataB')

        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.loadFromFileButton.clicked.connect(self.loadFromFile)
        self.ui.loadFromSharedMemoryButton.clicked.connect(self.loadFromMemory)

        self.setWindowTitle("SharedMemory Example")



    def loadFromFile(self):
        """ This slot function is called when the "Load Image From File..."
        button is pressed on the firs Dialog process.  First, it tests whether
        the process is already connected to a shared memory segment and, if so,
        detaches from that segment.  This ensures that we always start the
        example from the beginning if we run it multiple times with the same
        two Dialog processes.  After detaching from an existing shared memory
        segment, the user is prompted to select an image file.  The selected
        file is loaded into a QImage.  The QImage is displayed in the Dialog
        and streamed into a QBuffer with a QDataStream.  Next, it gets a new
        shared memory segment from the system big enough to hold the image data
        in the QBuffer, and it locks the segment to prevent the second Dialog
        process from accessing it.  Then it copies the image from the QBuffer
        into the shared memory segment.  Finally, it unlocks the shared memory
        segment so the second Dialog process can access it.  After self
        function runs, the user is expected to press the "Load Image from
        Shared Memory" button on the second Dialog process.
        """

        if self.sharedMemory.isAttached():
            self.detach()

        self.ui.label.setText("Select an image file")
        fileName = QFileDialog.getOpenFileName(self, None, None,
                "Images (*.png *.xpm *.jpg *.bmp)")
        path = fileName[0]

        image = QImage()
        print(path)
        if not image.load(path):
            self.ui.label.setText(
                    "Selected file is not an image, please select another.")
            return

        self.ui.label.setPixmap(QPixmap.fromImage(image))

        if SEND_RAW_FILE is 1:
            pass
#            # Load into shared memory.
#            f = open(path, 'rb', buffering=0)
#            f.seek(0, 2)
#            size=f.tell()
#            f.seek(0, 0)
#            fdata = f.read()
#            
##            randomByteArray = bytearray('ABC', 'utf-8')
##            print(randomByteArray)
##            print(type(randomByteArray))            
##            mv = memoryview(randomByteArray)
##            print(mv[0])
##            print(bytes(mv[0:2]))
##            print(list(mv[0:3]))
##
##            randomByteArray1 = bytearray(fdata)
##            print(randomByteArray1)
##            print(type(randomByteArray1))          
##            mv1 = memoryview(randomByteArray1)
##            print(mv1[0])
##            print(bytes(mv1[0:2]))
##            print(list(mv1[0:3]))
#            
#
#            
##            buf = QByteArray()
##            out = QDataStream(buf, QIODevice.WriteOnly)
##            out.setVersion(QDataStream.Qt_4_0)
##            out.writeBytes(fdata)
###            size = buf.size()            
##            print('size0=',buf.size())
##            print('qba data type=',type(buf.data()))
##            print('aaaa data=',buf[0])
###            print('fdata type=',type(fdata))
###            for i in range(0,8):
###                print('qba byteArray k=',buf.data[i])                    
###            for i in range(0,8):
###                print('qba byteArray k=',buf.data()[i])                                    
###            fdatas = fdata[:32]
###            print('f.size=',f.s)
###            for i in range(0,8):
###                print('fdatas k=',fdatas[i])        
###            view = memoryview(fdata) # CRASH! 
###            for i in range(0,8):
###                print('data k=',view[i])
            #size=4*6
        else:
            buf = QBuffer()
#            buf.open(QBuffer.ReadWrite)
#            print('buf.size()=',buf.size())
#            out = QDataStream(buf)
#            out << image
#            size = buf.size()
#            print('size0=',size)
#            print('fdata type=',type(buf.data()))
            
        #memtocopy = [0,1,2,3,4,5]
            
        if ROLE_IS_MASTER == 1:
            if not self.sharedMemory.create(8+2764800):
                self.ui.label.setText("Unable to create shared memory segment.")
                return
        else:
            if not self.sharedMemory.create(4 + 4 + 4 * 4 * 30):
                self.ui.label.setText("Unable to create shared memory segment.")
                return        
#        print('self.sharedMemory.size()=',self.sharedMemory.size())
#        print('size1=',size)
#        size = min(self.sharedMemory.size(), size)
#        print('size2=',size)

        rects=[]
        rect1 = [20, 20, 120, 100]
        rect2 = [120, 120, 420, 400]
        rect3 = [220, 220, 520, 500]
        rect4 = [320, 320, 620, 600]
#        rect1 = QRect(20, 20, 100, 80)
#        rect2 = QRect(120, 120, 300, 380)
#        rect3 = QRect(260, 260, 100, 80)
#        rect4 = QRect(580, 580, 100, 80)
        #eprint("ShrDataSlaveThread::read rects=",rects)
        #eprint("ShrDataSlaveThread::read rect4=",rect4)#PyQt5.QtCore.QRect(580, 580, 100, 80)
        #eprint("ShrDataSlaveThread::read type(rect4)=",type(rect4)) #<class 'PyQt5.QtCore.QRect'>
        rects.append(rect1)
        rects.append(rect2)
        rects.append(rect3)
        rects.append(rect4)
        
        idx = 0
        self.sharedMemory.lock()

        if SEND_RAW_FILE is 1:
            void_ptr = self.sharedMemory.data()
            sip_array = void_ptr.asarray()

            self._set_value(sip_array, idx, len(rects))
            print('sip_array[idx]=',self._get_value(sip_array, idx))
            idx += 4
            for rect in rects:
                self._set_value(sip_array, idx, rect[0])
                print('sip_array[idx]=',self._get_value(sip_array, idx))
                idx = idx + 4
                self._set_value(sip_array, idx, rect[1])
                print('sip_array[idx]=',self._get_value(sip_array, idx))
                idx = idx + 4
                self._set_value(sip_array, idx, rect[2])
                print('sip_array[idx]=',self._get_value(sip_array, idx))
                idx = idx + 4
                self._set_value(sip_array, idx, rect[3])
                print('sip_array[idx]=',self._get_value(sip_array, idx))
                idx = idx + 4                
                #sip_array[idx:4] = rect[:4]
                print('idx=',idx)
                print('rect[0]=',rect[0])
                print('rect[1]=',rect[1])
                print('rect[2]=',rect[2])
                print('rect[3]=',rect[3])

                #print('sip_array[idx:4]=',sip_array[idx:4])
                
            print('sip_array=',sip_array)
#            self.sharedMemory.data()[:size] = memoryview(struct.pack('=6i', *memtocopy))[:size]
#            #self.sharedMemory.data()[:size] = fdata[:size]
#            #self.sharedMemory.data()[:size] = view[:size]
        else:
            size=1
            # Copy image data from buf into shared memory area.
            self.sharedMemory.data()[:size] = buf.data()[:size]
        
        self.sharedMemory.unlock()
        #buf.close()
        #print('buf.size()=',buf.size())

#    def swap32(self, x):
#        return int.from_bytes(x.to_bytes(4, byteorder='little'), byteorder='big', signed=False)
        
    def swap32(self, x):
        return (((x << 24) & 0xFF000000) |
                ((x <<  8) & 0x00FF0000) |
                ((x >>  8) & 0x0000FF00) |
                ((x >> 24) & 0x000000FF))    

    def _set_value(self, x, startIdx, value):
        x[3+startIdx] = ((value & 0xFF000000) >> 24) 
        x[2+startIdx] = ((value & 0x00FF0000) >> 16) 
        x[1+startIdx] = ((value & 0x0000FF00) >> 8) 
        x[0+startIdx] = ((value & 0x000000FF)) 
        
    def _get_value(self, x, startIdx):
        return (((x[3+startIdx] << 24) & 0xFF000000) |
                ((x[2+startIdx] << 16) & 0x00FF0000) |
                ((x[1+startIdx] <<  8) & 0x0000FF00) |
                ((x[0+startIdx] ) & 0x000000FF))    
        
    def loadFromMemory(self):
        """ This slot function is called in the second Dialog process, when the
        user presses the "Load Image from Shared Memory" button.  First, it
        attaches the process to the shared memory segment created by the first
        Dialog process.  Then it locks the segment for exclusive access, copies
        the image data from the segment into a QBuffer, and streams the QBuffer
        into a QImage.  Then it unlocks the shared memory segment, detaches
        from it, and finally displays the QImage in the Dialog.
        """

        if not self.sharedMemory.attach():
            self.ui.label.setText(
                    "Unable to attach to shared memory segment.\nLoad an "
                    "image first.")
            return
 
#        if SEND_RAW_FILE is 1:    
#            buf = QBuffer()
#            ins = QDataStream(buf)   
#            ins.setVersion(QDataStream.Qt_5_0)                    
#        else:    
#            buf = QBuffer()
#            ins = QDataStream(buf)
#            
#            image = QImage()


#        f[:] = memoryview(self.sharedMemory.data()).cast('B')
#        for i in range(0,8):
#            print('data f=',f[i])        
        
        if SEND_RAW_FILE is 1:
            
            self.sharedMemory.lock()
            
            f1 = self.sharedMemory.data()
#            print('type of f1=',type(f1)) # <class 'sip.voidptr'>
            k1 = f1.asarray() # This returned the block of memory as a sip.array 
#            print('type of k1=',type(k1)) # <class 'sip.array'>
#            #view = memoryview(self.sharedMemory.data())
#            print('getsize = ',f1.getsize()) # 4096
#            
#            print('type of k1[0]=',type(k1[0])) # <class 'int'>
            frameIdx = self._get_value(k1, 2764800)
            print('frameIdx=',frameIdx)
            indicator = self._get_value(k1, 2764804)
            if frameIdx == 4660:
                print('great')
            if indicator == 0xFFFFFFFF:
                print('yes 2')
                
                
            
#            y = np.frombuffer(b'hello world',dtype='S1')
#            print('type y=',type(y)) #<class 'numpy.ndarray'>
#            print('y=',y) #[b'h' b'e' b'l' b'l' b'o' b' ' b'w' b'o' b'r' b'l' b'd']
#            print('y=',y.data) #<memory at 0x000001A8A40A54C8>
#            b=np.frombuffer(b'\x01\x02', dtype=np.uint8)
#            print('b=',b)
            #arr=[]
            #arr=k1[8:1280*720*3]
            #print('arr type=', type(arr))
            #print('arr ', arr)
            # 2764800
            b1=np.frombuffer(k1, dtype=np.uint8, count=2764808)#720*1280*3 + 8)
            print('b1 type=',type(b1))
            print('b1=',b1)
            print('shape=', b1.shape)
            print('shape0=', b1.shape[0])
            f=b1[0:2764800].reshape((720,1280,3))
            #f = np.frombuffer(b1[8:720*1280*3],dtype=np.uint8,count=720*1280*3).reshape((720,1280,3))
            #f=np.frombuffer(k1,dtype=np.uint8,count=720*1280*3).reshape((720,1280,3))
            print('f=',f)
            print('shape=', f.shape)
            print('shape0=', f.shape[0])            
            #im = f.reshape((1280, 720)) #notice row, column format
#            fd.close()
#           
#            skinCrCbHist = np.zeros((256, 256, 1), dtype = "uint8")
#            cv2.Mat mat = 
#            cv::Mat(image.height(), image.width(), CV_8UC3, (void*)image.constBits(), image.bytesPerLine());
            cv2.imshow('im', f)
#            cv2.waitKey()
#            cv2.destroyAllWindows()
#            print('k1[0]=',k1[0])
#            print('k1[1]=',k1[1])
#            print('k1[2]=',k1[2])
#            print('k1[3]=',k1[3])
#            print('k1[4]=',k1[4])
#            print('k1[5]=',k1[5])
#            print('k1[6]=',k1[6])
#            print('k1[7]=',k1[7])
#            frameIdx = k1[0:4]
#            indicator = k1[4:4]
#            print('type frameIdx=',type(frameIdx))
#            print('type indicator=',type(indicator))
#            print('frameIdx=',frameIdx)
#            print('indicator=',indicator)


            

            # python3
#            #a = array.array('i', range(6))
#            view = memoryview('iiiiii').cast('B')
#            #f[:] = buffer(a)
#            b = array.array('i')
#            b.frombytes(memoryview(f))
#            #b.fromstring(buffer(f))            
            # python2
            # k = struct.unpack('=6i', memoryview(shared_mem.data()))
            self.sharedMemory.unlock()
            

            #print('size()={0}'.format(self.sharedMemory.size()))
            
            
#            buf.setData(self.sharedMemory.constData())
#            buf.open(QBuffer.ReadOnly)
#            print('a1=',k[0])
#            print('a2=',k[1])
#            print('a3=',k[2])
#            print('a4=',k[3])
#            print('a5=',k[4])
#            print('a6=',k[5])
#            print('a7=',k[6])
#            print('a8=',k[7])
##            print('a6=',k[5])
##            print('a7=',k[6])
##            print('a8=',k[7])
##            print('a9=',k[8])
##            print('a10=',k[9])
##            print('a11=',k[10])
##            print('a12=',k[11])            
            
            
            #ins.skipRawData(4)
#            print('a1=',ins.readInt8())
#            print('a2=',ins.readInt8())
#            print('a3=',ins.readInt8())
#            print('a4=',ins.readInt8())
#            print('a5=',ins.readInt8())
#            print('a6=',ins.readInt8())
#            print('a7=',ins.readInt8())
#            print('a8=',ins.readInt8())
#            print('a9=',ins.readInt8())
#            print('a10=',ins.readInt8())
#            print('a11=',ins.readInt8())
#            print('a12=',ins.readInt8())
#            print('a13=',ins.readInt8())
#            print('a14=',ins.readInt8())
#            print('a15=',ins.readInt8())
#            print('a16=',ins.readInt8())
#            print('a17=',ins.readInt8())
#            print('a18=',ins.readInt8())
#            print('a19=',ins.readInt8())
#            print('a20=',ins.readInt8())
#            print('a21=',ins.readInt8())
#            print('a22=',ins.readInt8())            
#            frameIdx = ins.readUInt32()
#            length = ins.readUInt32()
#            frameIdx2 = ins.readUInt32()
##            QByteArray array2;
##        array2.reserve(4);
##        array2[0] = data[1];
##        array2[1] = data[2];
##        array2[2] = data[3];
##        array2[3] = data[4];
##
##        memcpy(&blockSize, array2, sizeof(int));
##            frameIdx = self.sharedMemory.constData()[:4]
##            indicator = self.sharedMemory.constData()[4:8]
##            #frameIdx2 = buf.data()[2:8]
##            
#            print('frameIdx=',frameIdx)
#            print('length=',length)
#            print('frameIdx2=',frameIdx2)
            
#            char *to = static_cast<char *>(self.sharedMemory.data());
#            const char *from = buffer.data().constData();
#            memcpy(to, from, qMin(size, sharedMemory->size()));
#            
#            #print('type.frameIdx=',type(frameIdx))
#            
#            rdata=self.sharedMemory.constData()
#            for i in range(0,8):
#                print('rdata k=',rdata[i])   
#                
#            rview = memoryview(self.sharedMemory.constData()) # CRASH! 
#            print('rview.shape=',rview.shape)
#            for i in range(0,8):
#                print('rview k=',rview[i])
#            print('rview tobyte t=',rview[0:8].tobytes())
            
        else:
            pass
#            buf.setData(self.sharedMemory.constData())
#            buf.open(QBuffer.ReadOnly)
#            ins >> image
        
#        print('shrsize=',self.sharedMemory.size())
#        print('buf.size()=',buf.size())
#        #frameIdx=buf.readData(8) 
#        frameIdx = buf.data()[:32]
#        frameIdx2 = self.sharedMemory.constData()[:32]
#        #frameIdx2 = buf.data()[2:8]
#        
#        print('frameIdx=',frameIdx)
#        print('frameIdx2=',frameIdx2)
        #print('type.frameIdx=',type(frameIdx))
        
        self.sharedMemory.unlock()
        
        self.sharedMemory.detach()
        
        #if SEND_RAW_FILE is not 1:
        #     self.ui.label.setPixmap(QPixmap.fromImage(image))

    def detach(self):
        """ This private function is called by the destructor to detach the
        process from its shared memory segment.  When the last process detaches
        from a shared memory segment, the system releases the shared memory.
        """

        if not self.sharedMemory.detach():
            self.ui.label.setText("Unable to detach from shared memory.")


if __name__ == '__main__':

    import sys

    app = QApplication(sys.argv)
    dialog = Dialog()
    dialog.show()
    sys.exit(app.exec_())