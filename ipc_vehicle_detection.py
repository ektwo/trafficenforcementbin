# -*- coding: utf-8 -*-
"""
Created on Mon Oct 29 03:43:44 2018

@author: s7908
"""
import time
import datetime
import logging
log_filename = datetime.datetime.now().strftime("log/tk%Y-%m-%d_%H_%M_%S.log")
logging.basicConfig(level=logging.DEBUG,
            format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
            datefmt='%m-%d %H:%M:%S',
            filename='mylog.log')

console = logging.StreamHandler()
console.setLevel(logging.INFO)

formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)




import shr_dataslave_thread
import shr_retmaster_thread

from PyQt5.QtCore import *
from PyQt5.QtCore import SIGNAL, SLOT


class IPCVehicleDetection(object):
    k_str_shared_memory_data = "sharedmemory_data"
    k_str_sem_src_data = "sem_src_data"
    k_str_sem_dst_data = "sem_dst_data"
    
    k_str_shared_memory_ret = "sharedmemory_ret"
    k_str_sem_src_ret = "sem_src_ret"
    k_str_sem_dst_ret = "sem_dst_ret"
   
    def __init__(self):
        self.dataslave_trd = shr_dataslave_thread.ShrDataSlaveThread
        self.dataslave_trd.Initialize(IPCVehicleDetection.k_str_shared_memory_data, 
                                      IPCVehicleDetection.k_str_sem_src_data, 
                                      IPCVehicleDetection.k_str_sem_dst_data, False)
        self.dataslave_trd.start()
        
        self.retmaster_trd = shr_retmaster_thread.ShrRetMasterThread
        self.retmaster_trd.Initialize(IPCVehicleDetection.k_str_shared_memory_ret, 
                                      IPCVehicleDetection.k_str_sem_src_ret, 
                                      IPCVehicleDetection.k_str_sem_dst_ret, False)
        self.retmaster_trd.start()
        
        self.connect(self.dataslave_trd, SIGNAL("sgDeliverBox"),
                     self.retmaster_trd, SLOT("soDeliverBox"))


    def __del__(self):
        self.sem.release()
        pass

    def ReceiveImage(self):
#        logging.info('ReceiveImage')
#        self.sem.acquire() 
##        if not self.sharedMemory.attach():
##            logging.info("Unable to attach to shared memory segment.\nLoad an "
##                "image first.")            
##            print("Unable to attach to shared memory segment.\nLoad an "
##                "image first.")
##            return
#        
#        #logging.info('ReceiveImage 2')
#        buf = QBuffer()
#        ins = QDataStream(buf)
#        image = QImage()
#    
#        #self.sharedMemory.lock()
#        buf.setData(self.sharedMemory.constData())
#        buf.open(QBuffer.ReadOnly)
#        ins >> image
#        #self.sharedMemory.unlock()
#        #self.sharedMemory.detach()
#    
#        #self.ui.label.setPixmap(QPixmap.fromImage(image))
#        cvImg = QImageToCvMat(image)
#        image.save(os.getcwd() + "qimg-from-cv_img.bmp")
#         
#        cv2.imshow("cvImg", cvImg)
#        
#        self.sem.release()
#        logging.info('ReceiveImage end')
#        #cv2.waitKey()
#        #cv2.destroyAllWindows()
        pass

if __name__ == '__main__':

    logging.info('__main__ go')
    ipc_vehicle_detection = IPCVehicleDetection()
    while True:
        ipc_vehicle_detection.ReceiveImage()
        logging.info('sleep 1')
        print("sleep 1")                    
        time.sleep(1)