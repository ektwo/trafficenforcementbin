# -*- coding: utf-8 -*-
"""
Created on Thu Nov  1 13:37:23 2018

@author: james.chan
"""
'''
Script to test traffic light localization and detection
'''
#from PIL import Image
#from matplotlib import pyplot as plt
import os
import sys
import time
from glob import glob
import numpy as np
import tensorflow as tf
import cv2
print('numpy version=',np.version.version)
print('opencv version=',cv2.__version__)
print('tensorflow version=',tf.__version__)

sys.path.append(os.getcwd())
#cwd = os.path.dirname(os.path.realpath(__file__))
model_path='./model/'
EMBEDDED_SOURCE=0
SHOW_VIDEO=0
SAVE_VIDEO=0

import datetime
import logging
log_filename = datetime.datetime.now().strftime("log/tk%Y-%m-%d_%H_%M_%S.log")
logging.basicConfig(level=logging.DEBUG,
            format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
            datefmt='%m-%d %H:%M:%S',
            filename='mylog.log')
console = logging.StreamHandler()
console.setLevel(logging.INFO)
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)
#import os
#os.environ["CUDA_VISIBLE_DEVICES"]="-1"    
#tf.device({'/device:GPU:0','/device:GPU:1'})#('/device:GPU:2')
#tf.device('/device:GPU:2')
#tf.device('/device:GPU:2')
#tf.device(['/device:GPU:1', '/device:GPU:2'])
# Uncomment the following two lines if need to use the visualization_tunitls
#os.chdir(cwd+'/models')
#from object_detection.utils import visualization_utils as vis_util

class CarDetector(object):
    def __init__(self):
        logging.info('init')
        self.car_boxes = []
        self.fps_list=[]#david        

        detect_model_name = 'ssd_mobilenet_v1_045_CarOnlyLabel_RZImg768_324_ssd300_300_Train3420_Test50_MixKilong_TaipeiImgs_186745_mAPP59'#bad than 044 frame is unstable

        PATH_TO_CKPT = model_path+detect_model_name + '/frozen_inference_graph.pb'
        
        # setup tensorflow graph
        self.detection_graph = tf.Graph()
        
        # configuration for possible GPU use
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        # load frozen tensorflow detection model and initialize 
        # the tensorflow graph
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
               serialized_graph = fid.read()
               od_graph_def.ParseFromString(serialized_graph)
               tf.import_graph_def(od_graph_def, name='')
               
            self.sess = tf.Session(graph=self.detection_graph, config=config)
            self.image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
              # Each box represents a part of the image where a particular object was detected.
            self.boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
              # Each score represent how level of confidence for each of the objects.
              # Score is shown on the result image, together with the class label.
            self.scores =self.detection_graph.get_tensor_by_name('detection_scores:0')
            self.classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
            self.num_detections =self.detection_graph.get_tensor_by_name('num_detections:0')
    
        ####################################################
        if EMBEDDED_SOURCE == 1:
            Dataset_Path='../DataSet/'
            #Video_file_name = 'DSCF4644.MOV'#'DSCF4644.MOV'#DSCF4615.MOV#DSCF4644.MOV#DSCF4447.MOV#DSCF4616.MOV
            #Video_file_name='rtsp://root:903fjjjjj@61.216.153.171/axis-media/media.amp'
            Video_file_name='rtsp://root:jolyone@61.216.153.174/axis-media/media.amp'
            if len(Video_file_name.split('rtsp'))>=2:
                self.cap = cv2.VideoCapture(Video_file_name)
            else:
                self.cap = cv2.VideoCapture(Dataset_Path+Video_file_name)
            if self.cap is None:
                logging.info('self.cap is empty')
            else:
                logging.info('self.cap is exist')
                
        if SAVE_VIDEO == 1:
            self.writer = cv2.VideoWriter('output.mp4', 0x00000021, 15.0, (1280,720))
            #fourcc = cv2.VideoWriter_fourcc('0x00000021')
            #self.writer = cv2.VideoWriter('output.mp4',fourcc, 20.0, (1280,720))

    def release(self):
        if EMBEDDED_SOURCE == 1:
            self.cap.release()
            cv2.destroyAllWindows()

    # Helper function to convert image into numpy array    
    def load_image_into_numpy_array(self, image):
         (im_width, im_height) = image.size
         return np.array(image.getdata()).reshape(
            (im_height, im_width, 3)).astype(np.uint8)       

    def box_normal_to_pixel(self, box, dim,score):#david
    
        height, width = dim[0], dim[1]
#        print('height=',height) #720
#        print('width=',width) #1280
#        print('int(box[0]*height)=',int(box[0]*height)) #top
#        print('int(box[1]*width)=',int(box[1]*width)) #left
#        print('int(box[2]*height=',int(box[2]*height)) #bottom
#        print('int(box[3]*width)=',int(box[3]*width)) #right
        box_info = [int(box[1]*width), 
                    int(box[0]*height), 
                    int(box[3]*width), 
                    int(box[2]*height)]        
#        box_info = [int(box[0]*height), 
#                    int(box[1]*width), 
#                    int(box[2]*height), 
#                    int(box[3]*width)]
#                    #int(score*100)]
        return box_info
        #return np.array(box_pixel)
        
    
    def process(self, image_arg):
        #start=time.time()
        if EMBEDDED_SOURCE == 1:
            #image=[]
            if self.cap.isOpened() is True:
                logging.info('self.cap.isOpened()')
                ret, image = self.cap.read()    
                #imageArg = copy.deepcopy(image)
                if ret == False:
                    logging.info('ret == False bye')
                    return self.car_boxes
            else:
                logging.info('self.cap.isOpened() !')
            
        if SHOW_VIDEO == 1:
            cv2.namedWindow("Image")
            cv2.imshow("Image", image)
            cv2.waitKey (1)
            
        if SAVE_VIDEO == 1:
            self.writer.write(image)

        with self.detection_graph.as_default():
            # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
            if EMBEDDED_SOURCE == 1:
                image_expanded = np.expand_dims(image, axis=0)
            else:
                image_expanded = np.expand_dims(image_arg, axis=0)
            #start=time.time()#david
            (boxes, scores, classes, num_detections) = self.sess.run(
                [self.boxes, self.scores, self.classes, self.num_detections],
                feed_dict={self.image_tensor: image_expanded})
            '''       
            end=time.time()#david
            self.count=self.count+1#david
            self.fps_list.append(round(end-start,2))#david
            print("xxxxxxxxxxxxxxxxxxxxxxxxmodel fps is ",round(end-start,2),self.fps_list)#david
            '''     

            boxes=np.squeeze(boxes)
            classes =np.squeeze(classes)
            scores = np.squeeze(scores)
            #print('boxes type=',type(boxes)) #<class 'numpy.ndarray'>
            #print('classes type=',type(classes)) #<class 'numpy.ndarray'>
            #print('scores type=',type(scores)) #<class 'numpy.ndarray'>
            cls = classes.tolist()
#            
#            # The ID for car is 3 
            car_boxes_tmp=[]            
            idx_vec = [i for i, v in enumerate(cls) if ((v==3) and (scores[i] > 0.1))]#85))]#0.3))]#0.1))]#david0.3))]            
            if len(idx_vec) != 0:
                #print('CarDetector get_localization detection! len(idx_vec)=',len(idx_vec))
                for idx in idx_vec:
                    if EMBEDDED_SOURCE == 1:
                        dim = image.shape[0:2]
                    else:
                        dim = image_arg.shape[0:2]
                    #print('idx={0} dim={1}'.format(idx, dim)) # 1 dim=(720, 1280)
                    #print('boxes[idx]=',boxes[idx]) #boxes[idx]= [ 0.63695949  0.0221467   0.97930986  0.9881047 ]
                    #print('scores[idx]=',scores[idx]) #0.140474    
                    box = self.box_normal_to_pixel(boxes[idx], dim, scores[idx])#david
#                    box_h = box[2] - box[0]
#                    box_w = box[3] - box[1]
#                    t=box[0]
#                    l=box[1]
#                    d=box[2]
#                    r=box[3]
                    car_boxes_tmp.append(box)
#                    if(box[3] - box[1] > 100): # width
#                        car_boxes_tmp.append(box)
#                    else:
#                        pass

                self.car_boxes = car_boxes_tmp
            else:
                self.car_boxes = []
            #start=time.time()    
            #end=time.time()
            #logging.info('time={0}'.format(end-start))
            #self.fps_list.append(round(end-start,2))#david
            #print("xxxxxxxxxxxxxxxxxxxxxxxxmodel fps is ",round(end-start,2),self.fps_list)#david
#        logging.info('done')
#        
#        retList=[]
#        retList.append(np.array([100, 200, 300, 400,50]))
#        retList.append(np.array([2, 3, 4, 5,50]))
#        
#        print(retList)
#        return retList

        return self.car_boxes    
    
    
		
#if __name__ == '__main__':
#    cam = cv2.VideoCapture(0)
#    img_counter = 0
#    while cam.isOpened():
#        ret, frame = cam.read()
#        cv2.imshow("test", frame)
#        if not ret:
#            break
#        key = cv2.waitKey(1) & 0xFF
#    
#        if key == 27: 
#            # press ESC to escape (ESC ASCII value: 27)
#            print("Escape hit, closing...")
#            break
#        elif key == 32:
#            # press Space to capture image (Space ASCII value: 32)
#            img_name = "opencv_frame_{}.png".format(img_counter)
#            cv2.imwrite(img_name, frame)
#            print("{} written!".format(img_name))
#            img_counter += 1
#        else:
#            pass
#    cam.release()
#    cv2.destroyAllWindows()    
