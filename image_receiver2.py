from __future__ import print_function
#import numpy as np
#import cv2
import sys, os
#import subprocess
#

import time
import datetime
import logging
log_filename = datetime.datetime.now().strftime("log/tk%Y-%m-%d_%H_%M_%S.log")
logging.basicConfig(level=logging.DEBUG,
            format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
            datefmt='%m-%d %H:%M:%S',
            filename='mylog.log')

console = logging.StreamHandler()
console.setLevel(logging.INFO)

formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)
#

#
#from multiprocessing import Process, Queue
#from PyQt4 import QtCore
#from MyJob import job_function
#
#
## Runner lives on the runner thread
#
#class Runner(QtCore.QObject):
#    """
#    Runs a job in a separate process and forwards messages from the job to the
#    main thread through a pyqtSignal.
#
#    """
#
#    msg_from_job = QtCore.pyqtSignal(object)
#
#    def __init__(self, start_signal):
#        """
#        :param start_signal: the pyqtSignal that starts the job
#
#        """
#        super(Runner, self).__init__()
#        self.job_input = None
#        start_signal.connect(self._run)
#
#    def _run(self):
#        queue = Queue()
#        p = Process(target=job_function, args=(queue, self.job_input))
#        p.start()
#        while True:
#            msg = queue.get()
#            self.msg_from_job.emit(msg)
#            if msg == 'done':
#                break
#
#
## Things below live on the main thread
#
#def run_job(input):
#    """ Call this to start a new job """
#    runner.job_input = input
#    runner_thread.start()
#
#
#def handle_msg(msg):
#    print(msg)
#    logging.info(msg)
#    if msg == 'done':
#        runner_thread.quit()
#        runner_thread.wait()
#
#
## Setup the OQ listener thread and move the OQ runner object to it
#runner_thread = QtCore.QThread()
#runner = Runner(start_signal=runner_thread.started)
#runner.msg_from_job.connect(handle_msg)
#runner.moveToThread(runner_thread)
#
#data_from_stderr = 0
#
#print(cv2.__version__)
#
##def sprintf(buf, fmt, *args):
##    buf.write(fmt % args)
#
class VideoCaptureReceiver():
    EXE_PATH = "bin/x64/Release/VideoCaptureTransmitter.exe"
    CMD_REQ_IMG = b'\x03'
    CMD_QUIT = b'\x04'

    NIR_IMG_WIDTH = 1280
    NIR_IMG_HEIGHT = 720
    NIR_IMG_CHANNEL = 3
    NIR_IMG_DEPTH = 1
    NIR_IMG_SIZE = NIR_IMG_WIDTH * NIR_IMG_HEIGHT * NIR_IMG_CHANNEL * NIR_IMG_DEPTH

    def __init__(self):
        self.proc = None
        #self.one = 1
        #self.one2 = 1

        
    def __del__(self):
        #logging.info('__del__')
        #print('__del__')
        self.stop()

    def open(self):
        logging.info("open")
        sys.stderr.write("impython_open")
        sys.stderr.flush()
        logging.info("impython_open finish")
#        if not self.proc:
#            print('os.path.exists(VideoCaptureReceiver.EXE_PATH)=',os.path.exists(VideoCaptureReceiver.EXE_PATH))
#            if os.path.exists(VideoCaptureReceiver.EXE_PATH):
#                if data_from_stderr == 1:
#                    self.proc = subprocess.Popen(VideoCaptureReceiver.EXE_PATH,
#                                                 stdin = subprocess.PIPE,
#                                                 stdout = subprocess.PIPE,
#                                                 stderr = sys.stderr)                    
#                else:                        
#                    self.proc = subprocess.Popen(VideoCaptureReceiver.EXE_PATH,
#                                                 stdin = subprocess.PIPE,
#                                                 stdout = subprocess.PIPE,
#                                                 stderr = sys.stderr)

    def is_opened(self):
        return True #self.proc != None

    def release(self):
        pass # always keep process alive, do nothing here

    def stop(self):
        #if self.proc:
        #self._send_cmd(VideoCaptureReceiver.CMD_QUIT)
        #self.proc.wait()
        #self.proc = None
        pass

    def _send_cmd(self, cmd):
        pass #logging.info("_send_cmd=")
        #
        #logging.info(cmd)
        #print("_send_cmd=", cmd)
#        try:
#            stdin = self.proc.stdin
#            stdin.write(cmd)
#            stdin.flush()
#            logging.info("_send_cmd finish")
#        except:
#            print("Failed to send cmd to vidacp transmitter process")

    def read(self):
        ret = False
        nir = None
        sys.stdin.write("impython_read")
        sys.stdin.flush()
        #logging.info("impython_read finish")
#        #if self.proc:
#        try:
#            self._send_cmd(VideoCaptureReceiver.CMD_REQ_IMG)
#            nir = self._read_nir()
#            ret = True
#        except:
#            raise RuntimeError("Failed to read img from vidcap transmitter process")

        return (ret, nir)

    def _read_nir(self):
#        logging.info('_read_nir')
#        if data_from_stderr == 1:
#            stdout = self.proc.stderr
#        else:
#            stdout = self.proc.stdout
#        
#        stdout.read()
#        nir = stdout.read(VideoCaptureReceiver.NIR_IMG_SIZE)
#        nir = np.frombuffer(nir, dtype=np.uint8, count=VideoCaptureReceiver.NIR_IMG_SIZE)
#        nir.shape = (VideoCaptureReceiver.NIR_IMG_HEIGHT,
#                         VideoCaptureReceiver.NIR_IMG_WIDTH,
#                         VideoCaptureReceiver.NIR_IMG_CHANNEL)        

        return nir

class VideoCapture():
    def __init__(self):
        self.opened = False
        self.remotecap = VideoCaptureReceiver()

    def open(self):
        if self.is_opened():
            return
        self.remotecap.open()

    def is_opened(self):
        return self.remotecap.is_opened()

    def release(self):
        if not self.is_opened():
            return
        self.remotecap.release()

    def read(self):
        return self.remotecap.read()


###################
##    Testing    ##
###################

def _show_img(img, title):
    if not img is None:
        cv2.imshow(title, img)
        #cv2.waitKey(1000)
        cv2.waitKey(33)

def _test(cnt, show_img):
    sys.stderr.write("hello welcome python")
    sys.stderr.flush()
    #logging.info("hello welcome python")    
    capture = VideoCapture()
    capture.open()
    
    while cnt < 3000:
        sys.stdin.write("hello welcome python" + str(cnt))
        sys.stdin.flush()
#        logging.info("hello welcome python")            
#        logging.info(cnt)
        #ret, nir = capture.read()
        #if ret:
            #if show_img:
            #    _show_img(nir, "NIR")
        #else:
        #    raise RuntimeError("Failed to capture image")
        cnt += 1

        #logging.info(cnt)
#
#    cv2.destroyAllWindows()
#
if __name__ == "__main__":
    cnt = 0
    show_img = True
    _test(cnt, show_img) 
    #_test(e_devicetype_etron_3d_camera, True, cnt, show_img)  # webcam

###
###print('hello,world')
##from PyQt5 import QtCore, QtGui, QtWidgets
##import sys
## 
##class Ui_Form(QtWidgets.QWidget):
##    def __init__(self):
##    	QtWidgets.QWidget.__init__(self)
##    	self.setupUi(self)
## 
##    def setupUi(self, Form):
##        Form.setObjectName("Form")
##        Form.resize(400, 300)
##        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
##        self.verticalLayout.setObjectName("verticalLayout")
##        self.pushButton = QtWidgets.QPushButton(Form)
##        self.pushButton.setObjectName("pushButton")
##        self.verticalLayout.addWidget(self.pushButton)
##        self.label = QtWidgets.QLabel(Form)
##        self.label.setText("")
##        self.label.setObjectName("label")
##        self.verticalLayout.addWidget(self.label)
## 
##        self.retranslateUi(Form)
##        QtCore.QMetaObject.connectSlotsByName(Form)
## 
##    def retranslateUi(self, Form):
##        _translate = QtCore.QCoreApplication.translate
##        Form.setWindowTitle(_translate("Form", "Form"))
##        self.pushButton.setText(_translate("Form", "Press Me!!!"))
##        self.pushButton.clicked.connect(self.printHello)
## 
##    def printHello(self):
##        print( "Hello World !")
## 
## 
##    if __name__ == '__main__':
##        app = QtWidgets.QApplication(sys.argv);
##        ex = Ui_Form(app);
##        ex.show();
##        sys.exit(app.exec_())


#!/usr/bin/env python
# -*- coding: utf-8 -*-
# test.py


#from cmd import Cmd
#import time, sys
#
#class MyPrompt(Cmd):
#    def do_help(self, args):
#        if len(args) == 0:
#            name = "   |'exec <testname>' or 'exec !<testnum>'\n   |0 BQ1\n   |1 BS1\n   |2 BA1\n   |3 BP1"
#        else:
#            name = args
#        print ("name = %s" % name)
#
#    def do_exec(self, args):
#        if (args == "!0"):
#            print ("   |||TEST BQ1_ACTIVE")
#        elif (args == "!1"):
#            print ("   |||TEST BS1_ACTIVE")
#        elif (args == "!2"):
#            print ("   |||TEST BA1_ACTIVE")
#        elif (args == "!3"):
#            print ("   |||TEST BP3_ACTIVE")
#        else:
#            print ("invalid input")
#        time.sleep(1)
#
#    def do_quit(self, args):
#        print ("Quitting.", file=sys.stderr)
#        return True
#
#if __name__ == '__main__':
#    prompt = MyPrompt()
#    prompt.use_rawinput = True
#    prompt.prompt = '$ '
#    prompt.cmdloop('Running test tool.')