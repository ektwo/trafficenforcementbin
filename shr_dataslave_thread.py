# -*- coding: utf-8 -*-
"""
Created on Sun Oct 28 16:27:10 2018

@author: s7908
"""
import sys
import time, threading
import cv2
import numpy as np

import PyQt5
#from PyQt5 import QtCore
from PyQt5.QtCore import QDateTime, QThread, QSystemSemaphore, QSharedMemory, QBuffer, QDataStream, QRect
from PyQt5.QtGui import QImage

import vd_model

import datetime
import logging

log_filename = datetime.datetime.now().strftime("log/tk%Y-%m-%d_%H_%M_%S.log")
logging.basicConfig(level=logging.DEBUG,
            format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
            datefmt='%m-%d %H:%M:%S',
            filename='mylog2.log')

console = logging.StreamHandler()
console.setLevel(logging.INFO)

formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)


PYTHREAD='''
class ShrDataSlaveThread(QtCore.QObject):
'''
QTTHREAD='''
class ShrDataSlaveThread(QtCore.QThread):
'''
USE_QTTHREAD=1

#def convertQImageToMat(incomingImage):
#    '''  Converts a QImage into an opencv MAT format  '''
#
#    incomingImage = incomingImage.convertToFormat(4)
#
#    width = incomingImage.width()
#    height = incomingImage.height()
#
#    ptr = incomingImage.bits()
#    ptr.setsize(incomingImage.byteCount())
#    arr = np.array(ptr).reshape(height, width, 4)  #  Copies the data
#    return arr

def eprint(*args, **kwargs):
    print(*args, file=sys.stdout, **kwargs)

def thread_info(msg):
    print(msg, int(PyQt5.QtCore.QThread.currentThreadId()),
          threading.current_thread().name)
#class ShrDataSlaveThread(QtCore.QObject):

class ShrDataSlaveThread(PyQt5.QtCore.QThread):
    USE_SYSSEMAPHORE=0
    SHOW_VIDEO=0
    TEST_MODE=0
    QIMAGE_MODE=0
    RAWIMAGE_MODE=1
    WIDTH=1280
    HEIGHT=720
    RAW_SIZE=WIDTH*HEIGHT*3 # 1280 * 720 * 3
    EKTWO_1105_FRAMERATE_TUNING=0
    OPEN_VD=True#True
    
    k_icmd_indicator = 0x00001117
    k_icmd_m_ask_start = 0x00000002
    k_icmd_m_ask_stop = 0x00000003
    k_icmd_m_streaming = 0x00000004
    k_icmd_s_readytostart = 0x00000101
    k_icmd_s_readytostop = 0x00000102
    k_icmd_s_readyforvd = 0x00000103

    sgDeliverBox = PyQt5.QtCore.pyqtSignal(int, int, list)    


    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent)
        #logging.info('ShrDataSlaveThread __init__')   
        self.running = False
        #logging.info('ShrDataSlaveThread __init__ 1')
        eprint('ShrDataSlaveThread::ShrDataSlaveThread __init__')
        self.frames = 0
        self.started = False
        #logging.info('wait tensorflow init ...')
        eprint('wait tensorflow init ...')
        if self.OPEN_VD:
            self.vd = vd_model.CarDetector()
        #logging.info('wait tensorflow init done...')
        eprint('wait tensorflow init done ...')
        self.shr_mem=None
        self.skipcnt=0
        #self.vd_is_opened=False
        

    def __del__(self):
        print('PPP del dataslave 1', flush=True)  
        #eprint('del dataslave 1')        
        if self.OPEN_VD:
            self.vd.release()
        if self.shr_mem:
            self.shr_mem.detach()
        print('PPP del dataslave 2', flush=True)  
        #eprint('PPP del dataslave 2')        
        if self.USE_SYSSEMAPHORE == 1:    
            self.sem_src.release()
            self.sem_dst.release()    
        print('PPP del dataslave 3', flush=True)                  
        #eprint('del dataslave 3')
        if self.SHOW_VIDEO == 1:
            cv2.destroyAllWindows()
        print('PPP del dataslave end', flush=True)  
        #eprint('del dataslave end')

    def initialize(self, shr_name, sem_src_name, sem_dst_name, create_or_not):
        #logging.info('ShrDataSlaveThread initialize')        
        ret = False
        #logging.info('ShrDataSlaveThread initialize 1')        
        eprint('IPC ShrDataSlaveThread::initialize')
        #logging.info('ShrDataSlaveThread::initialize')        
        self.shr_mem = QSharedMemory(shr_name)
        #logging.info('ShrDataSlaveThread initialize self.shr_mem={0}'.format(self.shr_mem))          
        if self.shr_mem:
            ret = self.shr_mem.attach()
            #logging.info('ShrDataSlaveThread initialize self.shr_mem.attach={0}'.format(ret))
            if ret != True:
                eprint('IPC ShrRetMasterThread::Initialize m_mem->attach error={0} ret={1}'.format(self.shr_mem.error(), self.shr_mem.errorString()))                    
            else:
                eprint('IPC ShrDataSlaveThread::initialize attach OK')
                if self.USE_SYSSEMAPHORE == 1:
                    if create_or_not == True:
                        self.sem_src = QSystemSemaphore(sem_src_name, 1, QSystemSemaphore.Create)
                        self.sem_dst = QSystemSemaphore(sem_dst_name, 0, QSystemSemaphore.Create)            
                    else:
                        self.sem_src = QSystemSemaphore(sem_src_name, 1, QSystemSemaphore.Open)
                        self.sem_dst = QSystemSemaphore(sem_dst_name, 0, QSystemSemaphore.Open)
                ret = True
        ret=True #temp
        return ret

    def start_timer(self):
        eprint('IPC ShrDataSlaveThread::start_timer')
        #logging.info('ShrDataSlaveThread::start_timer')  
        self.running = True
        if USE_QTTHREAD == 1:
            self.start()
        else:
            self._thread = threading.Thread(target=self.run)
            self._thread.start()        
        eprint('IPC ShrDataSlaveThread::start_timer end')
        #logging.info('ShrDataSlaveThread::start_timer end')     

    def stop(self):
        eprint('IPC ShrDataSlaveThread::stop')
        self.running = False

    def quit_from_cmd(self):
        rects=[]
        self.sgDeliverBox.emit(self.k_icmd_indicator, self.k_icmd_s_readytostop, rects)
        self.running = False

    def _get_value(self, x, startIdx):
        return (((x[3+startIdx] << 24) & 0xFF000000) |
                ((x[2+startIdx] << 16) & 0x00FF0000) |
                ((x[1+startIdx] <<  8) & 0x0000FF00) |
                ((x[0+startIdx] ) & 0x000000FF))  

    def read(self):
        #print('read',flush=True)
        #logging.info('ShrDataSlaveThread read')
        acquire = True
        if self.USE_SYSSEMAPHORE == 1:
            acquire = self.sem_dst.acquire()
        if acquire:
            #eprint('ShrDataSlaveThread::read m_semDst->acquire() pass')               
            if self.RAWIMAGE_MODE == 1:
                self.shr_mem.lock()
    
                void_ptr = self.shr_mem.data()
                if not void_ptr:
                    self.shr_mem.unlock()
                    return
                sip_array = void_ptr.asarray() # This returned the block of memory as a sip.array 
                
                frame_idx = self._get_value(sip_array, self.RAW_SIZE)
                cmd = self._get_value(sip_array, self.RAW_SIZE + 4)
                
                #logging.info('XXX frame_idx={0} cmd={1}'.format(frame_idx, cmd))
                
                #cv_np_array = np.array(cv_array)
                #cv_array=np.empty((720,1280,3))
                #eprint('IPC ShrDataSlaveThread.read frame_idx={0} self.k_icmd_indicator={1}'.format(frame_idx,self.k_icmd_indicator))
                if cmd == self.k_icmd_m_streaming:
                    if not self.started:
                        eprint('IPC ShrDataSlaveThread.read not self.started')
                        self.shr_mem.unlock()
                        if self.USE_SYSSEMAPHORE == 1:
                            self.sem_src.release() 
                        return

                    np_array=np.frombuffer(sip_array, dtype=np.uint8, count=2764800)#8)#720*1280*3 + 8)
                    cv_array=np_array[0:2764800].reshape((720,1280,3))
                  
                    self.shr_mem.unlock()
    
                    if self.SHOW_VIDEO == 1:
                        cv2.imshow('cv_array', cv_array)
                    
                    
                    
                    #start = time.time()
                    if self.OPEN_VD:
                        rects = self.vd.process(cv_array)
                    else:
                        rects = []
                    #end = time.time()
                    #print('PPP end-start=', end-start)
                    #if self.EKTWO_1105_FRAMERATE_TUNING==1:
                    
                    if self.skipcnt > 30:
                        if len(rects) > 0:
                            #eprint('IPC vd.process frame_idx={0} car_num={1} dur:{2}'.format(frame_idx,len(rects),end-start))
                            self.sgDeliverBox.emit(frame_idx, self.k_icmd_m_streaming, rects)                                    
                    else:
                        if self.skipcnt > 0:
                            self.skipcnt += 1
                        else:
                            self.skipcnt += 1
                            rects2 = []
                            print('PPP k_icmd_s_readyforvd', flush=True)
                            self.sgDeliverBox.emit(self.k_icmd_indicator, self.k_icmd_s_readyforvd, rects2)                                    
                            
                    if self.USE_SYSSEMAPHORE == 1:
                        self.sem_src.release()                    
                    
                elif cmd == self.k_icmd_m_ask_start:
                    if frame_idx == self.k_icmd_indicator:
                        eprint('IPC ShrDataSlaveThread.read emit k_icmd_indicator k_icmd_m_ask_start')
                        self.started = True

                        rects=[]
                        self.sgDeliverBox.emit(self.k_icmd_indicator, self.k_icmd_s_readytostart, rects)
                        self.shr_mem.unlock()
                        if self.USE_SYSSEMAPHORE == 1:
                            self.sem_src.release()                        
                        return
                    
                elif cmd == self.k_icmd_m_ask_stop:
                    print(' ***** k_icmd_m_ask_stop',flush=True)
                    if frame_idx == self.k_icmd_indicator:
                        eprint('IPC ShrDataSlaveThread.read k_icmd_indicator k_icmd_m_ask_stop')
                        rects = []
                        self.sgDeliverBox.emit(self.k_icmd_indicator, self.k_icmd_s_readytostop, rects)
                        self.shr_mem.unlock()
                        if self.USE_SYSSEMAPHORE == 1:
                            self.sem_src.release()                        
                        self.running = False
                        return                        
            elif self.QIMAGE_MODE == 1:
                pass
            #                    buf = QBuffer()
            #                    ins = QDataStream(buf)
            #                    image = QImage()
            #                    
            #                    self.shr_mem.lock()
            #                    
            #                    buf.setData(self.shr_mem.constData())
            #                    buf.open(QBuffer.ReadOnly)
            #                    ins >> image               
            #                    
            #                    
            ##                    height, width, bytesPerComponent = image.shape
            ##                    bytesPerLine = 3 * width
            ##                    cv2.cvtColor(image, cv2.COLOR_BGR2RGB, image)    
            #                    
            #                    #QImg = QImage(Img.data, width, height, bytesPerLine,QImage.Format_RGB888)
            #                    
            #                    self.shr_mem.unlock()
            #                    
            #                    cvImg = convertQImageToMat(image)
            #                    cv2.imshow('cvImg', cvImg)
            #            
            #                    if self.USE_SYSSEMAPHORE == 1:
            #                        self.sem_src.release()


        #eprint("ShrDataSlaveThread::read m_semSrc->release");
    
#    def start(self):
#        self._thread = threading.Thread(target=self.run)
#        self._thread.start()

    def run(self):
        eprint('ShrDataSlaveThread run in')
        while self.running:
            self.read()
            if self.SHOW_VIDEO == 1:
                key = cv2.waitKey(50) & 0xFF
                if key == 27:
                    self.quit_from_cmd()
                    print("Escape hit, closing...")
                    break                    
                else:
                    pass
            else:                
                time.sleep(0.025)
        if self.SHOW_VIDEO == 1:
            cv2.destroyAllWindows()

        print('PPP ShrDataSlaveThread run over',flush=True)        
        #eprint('ShrDataSlaveThread run over')
        #logging.info('ShrDataSlaveThread run over')    