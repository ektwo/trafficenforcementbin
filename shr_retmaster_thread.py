# -*- coding: utf-8 -*-
"""
Created on Sun Oct 28 16:27:33 2018

@author: s7908
"""
import time, threading
#from PyQt5 import QtCore
import PyQt5
from PyQt5.QtCore import QSystemSemaphore, QSharedMemory, QBuffer, QDataStream, QRect

import os
import sys

import datetime
import logging
log_filename = datetime.datetime.now().strftime("log/tk%Y-%m-%d_%H_%M_%S.log")
logging.basicConfig(level=logging.DEBUG,
            format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
            datefmt='%m-%d %H:%M:%S',
            filename='mylog2.log')

console = logging.StreamHandler()
console.setLevel(logging.INFO)

formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

def eprint(*args, **kwargs):
    print(*args, file=sys.stdout, **kwargs)
    
def thread_info(msg):
    print(msg, int(PyQt5.QtCore.QThread.currentThreadId()),
          threading.current_thread().name)

class ShrRetMasterThread(PyQt5.QtCore.QThread):
    #sig = PyQt5.QtCore.pyqtSignal()
    sgQuit = PyQt5.QtCore.pyqtSignal()
    USE_SYSSEMAPHORE=0
    TEST_MODE=0
    EKTWO_1105_FRAMERATE_TUNING=0
    
    k_icmd_indicator = 0x00001117
    k_icmd_m_ask_stop = 0x00000003
    k_icmd_m_streaming = 0x00000004
    k_icmd_s_readytostart = 0x00000101
    k_icmd_s_readytostop = 0x00000102
    k_icmd_s_readyforvd = 0x00000103
    k_icmd_s_streaming = 0x00000104
    
    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent)
        logging.info('ShrRetMasterThread __init__')  
        self.running = True
        self.m_askAbort = False
        self.shrmem_size = 4 + 4 + 4 + 4 * 4 * 30
        self.shr_mem=None
        logging.info('ShrRetMasterThread __init__ 1')
        eprint('ShrRetMasterThread::ShrRetMasterThread')
         
    def __del__(self):
        print('PPP del retmaster 1', flush=True)  
        #eprint('PPP del retmaster 1')
        if self.shr_mem:
            self.shr_mem.detach()
        print('PPP del retmaster 2', flush=True)              
        #eprint('del retmaster 2')
        if self.USE_SYSSEMAPHORE == 1:    
            self.sem_src.release()
            self.sem_dst.release()
        print('PPP del retmaster 3', flush=True)                  
        #eprint('del retmaster end')
        pass        
        
    def initialize(self, shr_name, sem_src_name, sem_dst_name, create_or_not):
        #logging.info('ShrRetMasterThread initialize')        
        ret = False
        #logging.info('ShrRetMasterThread initialize 1')   
        eprint('IPC ShrRetMasterThread::initialize')
        #logging.info('ShrDataSlaveThread::initialize')        
        self.shr_mem = QSharedMemory(shr_name)
        logging.info('IPC ShrRetMasterThread initialize self.shr_mem={0}'.format(self.shr_mem)) 
        if self.shr_mem:
            ret = self.shr_mem.attach()
            #logging.info('ShrRetMasterThread initialize self.shr_mem.attach={0}'.format(ret))
            if ret != True:
                eprint('IPC ShrRetMasterThread::Initialize m_mem->attach error={0} ret={1}'.format(self.shr_mem.error(), self.shr_mem.errorString()))
            else:
                eprint('IPC ShrRetMasterThread::initialize attach OK')
                if self.USE_SYSSEMAPHORE == 1:
                    if create_or_not == True:
                        self.sem_src = QSystemSemaphore(sem_src_name, 1, QSystemSemaphore.Create)
                        self.sem_dst = QSystemSemaphore(sem_dst_name, 0, QSystemSemaphore.Create)            
                    else:
                        self.sem_src = QSystemSemaphore(sem_src_name, 1, QSystemSemaphore.Open)
                        self.sem_dst = QSystemSemaphore(sem_dst_name, 0, QSystemSemaphore.Open)
                ret = True
        ret=True #temp
        return ret   

    def start_timer(self):
        eprint('IPC ShrRetMasterThread::start_timer')
        #logging.info('ShrRetMasterThread::start_timer')  
        self.running = True
        self.start()
        eprint('IPC ShrRetMasterThread::start_timer end')
        #logging.info('ShrRetMasterThread::start_timer end')    

    def stop(self):
        eprint('IPC ShrRetMasterThread::stop')
        self.running = False

    def _get_value(self, x, startIdx):
        return (((x[3+startIdx] << 24) & 0xFF000000) |
                ((x[2+startIdx] << 16) & 0x00FF0000) |
                ((x[1+startIdx] <<  8) & 0x0000FF00) |
                ((x[0+startIdx] ) & 0x000000FF))  


    def _set_value(self, x, startIdx, value):
        x[3+startIdx] = ((value & 0xFF000000) >> 24) 
        x[2+startIdx] = ((value & 0x00FF0000) >> 16) 
        x[1+startIdx] = ((value & 0x0000FF00) >> 8) 
        x[0+startIdx] = ((value & 0x000000FF)) 

    def soDeliverBoxG(self, frame_idx, cmd, rects):
        #eprint('soDeliverBoxG frame_idx=', frame_idx)
        #eprint('soDeliverBoxG quitSignal=', quitSignal)
        #eprint('soDeliverBoxG len(rects)=', len(rects))
        
        if self.EKTWO_1105_FRAMERATE_TUNING==1:
            start = time.time()
        acquire = True
        if self.USE_SYSSEMAPHORE == 1:
            acquire = self.sem_src.acquire()
        if acquire:
            #logging.info('ShrRetMasterThread::soDeliverBox quit signal 2')
            #eprint('IPC ShrRetMasterThread.soDeliverBoxG frame_idx={0} self.k_icmd_indicator={1}'.format(frame_idx,self.k_icmd_indicator))
            if cmd == self.k_icmd_m_streaming:
                rect_cnt = len(rects)
                if rect_cnt >= 30 or rect_cnt == 0:
                    return
    
                idx = 0
                
                self.shr_mem.lock()
                
                void_ptr = self.shr_mem.data()
                if not void_ptr:
                    self.shr_mem.unlock()
                    return
                sip_array = void_ptr.asarray()
                
                #self._set_value(sip_array, idx, rect_cnt)
                self._set_value(sip_array, 0, rect_cnt)
                self._set_value(sip_array, 4, self.k_icmd_s_streaming)
                self._set_value(sip_array, 8, frame_idx)
                #print('sip_array[idx]=',self._get_value(sip_array, idx))
                idx = 12
                for rect in rects:
                    #print('rect type=',type(rect)) #<class 'list'>
                    #print('rect[0] type=',type(rect[0])) #<class 'int'>
                    self._set_value(sip_array, idx, rect[0])
                    #eprint('IPC soDeliverBoxG [{0}]sip_array[{1}{2}]={3}'.format(frame_idx,rect_cnt,idx,self._get_value(sip_array, idx)))
                    idx = idx + 4
                    self._set_value(sip_array, idx, rect[1])
                    #eprint('IPC soDeliverBoxG [{0}]sip_array[{1}{2}]={3}'.format(frame_idx,rect_cnt,idx,self._get_value(sip_array, idx)))
                    idx = idx + 4
                    self._set_value(sip_array, idx, rect[2])
                    #eprint('IPC soDeliverBoxG [{0}]sip_array[{1}{2}]={3}'.format(frame_idx,rect_cnt,idx,self._get_value(sip_array, idx)))
                    idx = idx + 4
                    self._set_value(sip_array, idx, rect[3])
                    #eprint('IPC soDeliverBoxG [{0}]sip_array[{1}{2}]={3}'.format(frame_idx,rect_cnt,idx,self._get_value(sip_array, idx)))
                    idx = idx + 4
                    #sip_array[idx:4] = rect[:4]
    #                    print('idx=',idx)
    #                    print('rect[0]=',rect[0])
    #                    print('rect[1]=',rect[1])
    #                    print('rect[2]=',rect[2])
    #                    print('rect[3]=',rect[3])
    
                self.shr_mem.unlock()
                if self.EKTWO_1105_FRAMERATE_TUNING==1:
                    end = time.time() # end-start ~=0
                    eprint('IPC ShrRetMasterThread frame_idx={0} car_num={1} dur:{2} end:{3}'.format(frame_idx,len(rects),end-start,end))
                if self.USE_SYSSEMAPHORE == 1:
                    self.sem_dst.release()
                
            elif cmd == self.k_icmd_s_readytostart:
                if frame_idx == self.k_icmd_indicator:
                    eprint('PPP ShrRetMasterThread.soDeliverBoxG k_icmd_indicator k_icmd_s_readytostart')
                    self.shr_mem.lock()
                    
                    void_ptr = self.shr_mem.data()
                    if void_ptr:
                        sip_array = void_ptr.asarray()
                        self._set_value(sip_array, 0, self.k_icmd_indicator)
                        self._set_value(sip_array, 4, self.k_icmd_s_readytostart)
                        self._set_value(sip_array, 8, frame_idx)
                        #eprint('IPC ShrRetMasterThread.soDeliverBoxG sip_array[0]=',self._get_value(sip_array, 0))
                        #eprint('IPC ShrRetMasterThread.soDeliverBoxG sip_array[4]=',self._get_value(sip_array, 4))
                        
                    self.shr_mem.unlock()
                    if self.USE_SYSSEMAPHORE == 1:
                        self.sem_dst.release()
                return
                
            elif cmd == self.k_icmd_s_readytostop:
                if frame_idx == self.k_icmd_indicator:
                    eprint('PPP ShrRetMasterThread.soDeliverBoxG k_icmd_indicator k_icmd_s_readytostop')
                    self.shr_mem.lock()
                    
                    void_ptr = self.shr_mem.data()
                    if void_ptr:
                        sip_array = void_ptr.asarray()
                        self._set_value(sip_array, 0, self.k_icmd_indicator)
                        self._set_value(sip_array, 4, self.k_icmd_s_readytostop)
                        self._set_value(sip_array, 8, frame_idx)
    
                    self.shr_mem.unlock()
                    if self.USE_SYSSEMAPHORE == 1:
                        self.sem_dst.release()
                        
                    self.m_running = False
                    self.sgQuit.emit() 
                return
            elif cmd == self.k_icmd_s_readyforvd:
                if frame_idx == self.k_icmd_indicator:
                    eprint('PPP ShrRetMasterThread.soDeliverBoxG k_icmd_indicator k_icmd_s_readyforvd')
                    self.shr_mem.lock()
                    
                    void_ptr = self.shr_mem.data()
                    if void_ptr:
                        sip_array = void_ptr.asarray()
                        self._set_value(sip_array, 0, self.k_icmd_indicator)
                        self._set_value(sip_array, 4, self.k_icmd_s_readyforvd)
                        self._set_value(sip_array, 8, frame_idx)
    
                    self.shr_mem.unlock()
                    if self.USE_SYSSEMAPHORE == 1:
                        self.sem_dst.release()

                return            

                
            #eprint("ShrRetMasterThread::soDeliverBox sem_dst.release()")                
            #logging.info('ShrRetMasterThread::soDeliverBox sem_dst.release()')
            #buf = QBuffer()
#                out = QDataStream(buf)
#                
#                self.shr_mem.lock()
#                
#                if frame_idx == -1 and quitSignal:
#                    logging.info('ShrRetMasterThread::soDeliverBox quit signal 1')
#                    #eprint('ShrRetMasterThread::soDeliverBox quit signal 1')
#                    
#                    data = self.shr_mem.constData()
#                    buf.setData(data)
#                    buf.open(QBuffer.WriteOnly)
#                    
#                    out.writeInt32(-1)
#                    out.writeInt32(0)
#          
#                    self.shr_mem.unlock()
#                    self.sem_dst.release()
#                    self.m_running = False
#                    self.sgQuit.emit()
#                    logging.info('ShrRetMasterThread::soDeliverBox quit signal 2')
#                    #eprint('ShrRetMasterThread::soDeliverBox quit signal 2')    
#                    return
#                
#                rect_cnt = len(rects)
#                #logging.info('ShrRetMasterThread::soDeliverBox sem_src.acquire() ok rects.size()={0}'.format(rect_cnt))
#                #eprint("ShrRetMasterThread::soDeliverBox sem_src.acquire() ok rects.size()=", rect_cnt)
#                
#                if rect_cnt >= 30:
#                    return
#    
#                data = self.shr_mem.constData()
#                buf.setData(data)
#                buf.open(QBuffer.WriteOnly)
#                out.writeInt32(frame_idx)
#                out.writeInt32(rect_cnt)
            
            #logging.info('ShrRetMasterThread frame_idx={0}'.format(frame_idx))
            #logging.info('soDeliverBox data0={0} data1={1} data2={2} data3={3}'.
            #             format(data[0],data[1],data[2],data[3]))
            #eprint("data0={0} data1={1} data2={2} data3={3}".format(data[0],data[1],data[2],data[3]))

#                rc = QRect()
#                for rect in rects:
#                    rc = rect
#                    
#                    #logging.info("rc.left()={0} data1={1} data2={2} data3={3}".
#                    #             format(rc.left(),rc.top(),rc.left() + rc.width(),rc.top() + rc.height()))
#                    
#                    #eprint("rc.left()={0} data1={1} data2={2} data3={3}".
#                    #       format(rc.left(),rc.top(),rc.left() + rc.width(),rc.top() + rc.height()))
#                    out.writeInt32(rc.left())
#                    out.writeInt32(rc.top())
#                    out.writeInt32(rc.left() + rc.width())
#                    out.writeInt32(rc.top() + rc.height())              
#    
#                self.shr_mem.unlock()
#                if self.USE_SYSSEMAPHORE == 1:
#                    self.sem_dst.release()
                
            #eprint("ShrRetMasterThread::soDeliverBox sem_dst.release()")

    def run(self):
        eprint('PPP ShrRetMasterThread run in')
        while self.running:
            time.sleep(0.025)
            #thread_info('qt:run')
            #self.sig.emit()
        self.running = False
        eprint('PPP ShrRetMasterThread run over')
        #logging.info('ShrRetMasterThread run over')                