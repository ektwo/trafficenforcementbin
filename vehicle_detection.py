# -*- coding: utf-8 -*-
"""
Created on Mon Oct 29 03:43:44 2018

@author: s7908
"""
import sys, time, threading
import PyQt5
from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5.QtCore import QCoreApplication, QThread, pyqtSignal, QDateTime
from PyQt5.QtWidgets import QApplication,  QDialog, QLineEdit

import shr_dataslave_thread
import shr_retmaster_thread


import datetime
import logging

log_filename = datetime.datetime.now().strftime("log/tk%Y-%m-%d_%H_%M_%S.log")
logging.basicConfig(level=logging.DEBUG,
            format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
            datefmt='%m-%d %H:%M:%S',
            filename='mylog2.log')

console = logging.StreamHandler()
console.setLevel(logging.INFO)

formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

def eprint(*args, **kwargs):
    print(*args, file=sys.stdout, **kwargs)
    #print(*args, file=sys.stdout, **kwargs)

def thread_info(msg):
    print(msg, int(QtCore.QThread.currentThreadId()),
          threading.current_thread().name)
    
from PyQt5 import QtCore    


def quit_func():
    print('cool 2', flush=True)
    QtCore.QCoreApplication.instance().quit()    
    
#class IPCVehicleDetection(QtCore.QObject):#QtWidgets.QWidget):
class IPCVehicleDetection(QtWidgets.QWidget):    
    ####################################################
    ####################################################
    ####################################################
    k_str_shared_memory_data = "sharedmemory_data10"
    k_str_sem_src_data = "sem_src_data"
    k_str_sem_dst_data = "sem_dst_data"

    k_str_shared_memory_ret = "sharedmemory_ret10"
    k_str_sem_src_ret = "sem_src_ret"
    k_str_sem_dst_ret = "sem_dst_ret"
    ####################################################
    ####################################################
    ####################################################
    OPEN_DATASLAVE_THREAD=1
    OPEN_RETMASTER_THREAD=1
    k_icmd_indicator = 0x00001117
    k_icmd_s_readytostart = 0x00000101
    
    sgExit = QtCore.pyqtSignal()
    sgQuitTest = QtCore.pyqtSignal()
    
    def __init__(self):#, parent=None):
        super(self.__class__, self).__init__()
        #super(IPCVehicleDetection, self).__init__()
        #logging.info('IPCVehicleDetection __init__')
        
        if self.OPEN_DATASLAVE_THREAD == 1:
            self.dataslave = shr_dataslave_thread.ShrDataSlaveThread()
            print('IPCVehicleDetection __init__ 0', flush=True)
            
        if self.OPEN_RETMASTER_THREAD == 1:
            self.retmaster = shr_retmaster_thread.ShrRetMasterThread()
            print('IPCVehicleDetection __init__ 1', flush=True)
            
        if self.OPEN_DATASLAVE_THREAD == 1:
            self.dataslave_inited = self.dataslave.initialize(self.k_str_shared_memory_data,
                                                              self.k_str_sem_src_data,
                                                              self.k_str_sem_dst_data,
                                                              False)
        if self.OPEN_RETMASTER_THREAD == 1:
            print('IPCVehicleDetection __init__ 2', flush=True)
            self.retmaster_inited = self.retmaster.initialize(self.k_str_shared_memory_ret,
                                                              self.k_str_sem_src_ret,
                                                              self.k_str_sem_dst_ret,
                                                              False)        
            print('IPCVehicleDetection __init__ 3', flush=True)
        #self.dataslave.sig.connect(self.pyslot)
        #self.dataslave.sgDeliverBox.connect(self.soDeliverBox)
        if self.OPEN_DATASLAVE_THREAD == 1 and self.OPEN_RETMASTER_THREAD == 1:    
            self.dataslave.sgDeliverBox.connect(self.retmaster.soDeliverBoxG)
            print('IPCVehicleDetection __init__ 4', flush=True)
        
        if self.OPEN_DATASLAVE_THREAD == 1:    
            self.dataslave.start_timer()
            print('IPCVehicleDetection __init__ 5', flush=True)
            #if self.dataslave_inited:
            #    self.dataslave.start_timer()
            print('IPCVehicleDetection __init__ 6', flush=True)
        
        if self.OPEN_RETMASTER_THREAD == 1:
            #self.retmaster.sig.connect(self.qtslot)
            print('IPCVehicleDetection __init__ 7', flush=True)
            self.retmaster.sgQuit.connect(self.soQuit)
            print('IPCVehicleDetection __init__ 8', flush=True)
            self.retmaster.start_timer()   
        
        #self.sgQuitTest.connect(self.soQuit)

        
        self.running = True
        print('IPC VehicleDetection k_icmd_s_readytostart', flush=True)        
        rects=[]
        if self.OPEN_RETMASTER_THREAD == 1:
            self.dataslave.sgDeliverBox.emit(self.k_icmd_indicator, self.k_icmd_s_readytostart, rects)
        
        # self.timer2 = QtCore.QTimer()
        # self.timer2.timeout.connect(self.soQuit)
        # self.timer2.start(5000)
        
        #if self.retmaster_inited:
        #    self.retmaster.start_timer()   

#    def pyslot(self):
#        thread_info('py:slot')
        
#    def soDeliverBox(self, frameIdx, quitSignal, rects):
#        logging.info('IPCVehicleDetection soDeliverBox ')
#        eprint('soDeliverBox frameIdx=', frameIdx)
#        eprint('soDeliverBox quitSignal=', quitSignal)
#        eprint('soDeliverBox len(rects)=', len(rects))

#    def qtslot(self):
#        thread_info('qt:slot')
        
    def __del__(self):
        print('IPC __del__ 1', flush=True)
        #eprint('IPCVehicleDetection __del__')
#        if self.OPEN_DATASLAVE_THREAD == 1:
#            del self.dataslave
#            
#        if self.OPEN_RETMASTER_THREAD == 1:
#            del self.retmaster      
        print('IPC __del__ 2', flush=True)
        #self.soQuit()
        #self.soExit()
        #QtCore.QCoreApplication.instance().quit() 
        print('PPP IPCVehicleDetection soQuit', flush=True)
        self.soQuit()
        self.soExit()
        
        
    def soQuit(self):
        #QtCore.QCoreApplication.instance().quit() 
        print('PPP IPCVehicleDetection soQuit', flush=True)
        self.running = False
        if self.OPEN_DATASLAVE_THREAD == 1:
            if self.dataslave:
                self.dataslave.stop()
                self.dataslave.quit()
                self.dataslave.wait()
            
        if self.OPEN_RETMASTER_THREAD == 1:
            if self.retmaster:
                self.retmaster.stop()
                self.retmaster.quit()
                self.retmaster.wait()            
        print('PPP IPCVehicleDetection soQuit 2', flush=True)
        #time.sleep(0.5)
        print('PPP IPCVehicleDetection soQuit 3', flush=True)
        #self.sgExit.emit()


    def soExit(self):
        print('PPP IPCVehicleDetection soExit 1', flush=True)
        if self.OPEN_DATASLAVE_THREAD == 1:
            if self.dataslave:
                del self.dataslave
                self.dataslave=None
        print('PPP IPCVehicleDetection soExit 2', flush=True)
        if self.OPEN_RETMASTER_THREAD == 1:
            if self.retmaster:
                del self.retmaster
                self.retmaster=None
        print('PPP IPCVehicleDetection soExit 3', flush=True)      
        #time.sleep(2)
        #sys.exit(1)
        #eprint('soExit will do sys.exit 3') 
        
        #eprint('soExit will do sys.exit 4') 
        #QtWidgets.QApplication.quit()
        #QtCore.QCoreApplication.instance().quit()  
        #time.sleep(0.5)
        #QtCore.QCoreApplication.instance().quit()  
        # try:
        #     quit()
        #     #sys.exit(0)
        #     #QtWidgets.QApplication.quit() 
        # except:
        #     print('Program is dead.')
        # finally:
        #     print('clean-up')
        #     QtWidgets.QApplication.quit() 
#        eprint('soExit finish sys.exit')    
#        try:
#            sys.exit(0)
#        except:
#            print('Program is dead.')
#        finally:
#            print('clean-up')
        
#        eprint('soExit finish sys.exit')
        
#
import signal

def sigint_handler(*args):
    """Handler for the SIGINT signal."""
#    sys.stderr.write('\r')
#    if QMessageBox.question(None, '', "Are you sure you want to quit?",
#                            QMessageBox.Yes | QMessageBox.No,
#                            QMessageBox.No) == QMessageBox.Yes:
    #ipc_vd.dataslave.quit_from_cmd()
    print('DDDDD sigint_handler DDDDD', flush=True)
    QtWidgets.QApplication.quit()
#
#def handleIntSignal(signum, frame):
#    '''Ask app to close if Ctrl+C is pressed.'''
#    PyQt4.QtGui.qApp.closeAllWindows()
class BackendThread(QThread):
    update_date = pyqtSignal(str)

    def run(self):
        while True:
            #data = QDateTime.currentDateTime()
            #currTime = data.toString("yyyy-MM-dd hh:mm:ss")
            #self.update_date.emit( str(currTime) )
            time.sleep(5)

class Window(QDialog):
    def __init__(self):
        QDialog.__init__(self)
        self.setWindowTitle('vd_model')
        self.setWindowFlags(QtCore.Qt.WindowCloseButtonHint | QtCore.Qt.WindowMinimizeButtonHint)
        #self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)        
        self.resize(250, 100)
        self.input = QLineEdit(self)
        self.input.resize(250, 100)
        self.initUI()

    def initUI(self):
        self.backend = BackendThread()
        self.backend.update_date.connect(self.handleDisplay)
        self.backend.start()

    def handleDisplay(self, data):
        self.input.setText(data)

if __name__ == '__main__':
    
    USE_QTAPP=1
    if USE_QTAPP == 1:   
        signal.signal(signal.SIGINT, sigint_handler)
        eprint('AAAAAAAAAAAAAAAAAAAAAAAA')
        #logging.info('AAAAAAAAAAAAAAAAAAAAAAAA')
        if not QtWidgets.QApplication.instance():
            app = QtWidgets.QApplication(sys.argv)
        else:
            app = QtWidgets.QApplication.instance()
        
        ipc_vd = IPCVehicleDetection()
        window = Window()
        window.show()
        sys.exit(app.exec_())

        #window.start()
        #window.setGeometry(600, 100, 300, 200)
        #window.show()
        #thread_info('main')
        # w = QtWidgets.QWidget()
        # w.resize(250, 150)
        # w.move(300, 300)
        # w.setWindowTitle('Simple')
        # w.show()
        #sys.exit(app.exec_())
    else:
        #signal.signal(signal.SIGINT, sigint_handler)
        print('AAAAAAAAAAAAAAAAAAAAAAAA', flush=True)
        #logging.info('AAAAAAAAAAAAAAAAAAAAAAAA')
        if not QtWidgets.QApplication.instance():
            app = QtWidgets.QApplication(sys.argv)
        else:
            app = QtWidgets.QApplication.instance()
        
        ipc_vd = IPCVehicleDetection()
        ipc_vd.sgExit.connect(ipc_vd.soExit)
        #logging.info('ipc_vd ok')
        print('AAAAAAAAAAAAAAAAAAAAAAAA ipc_vd ok', flush=True)
        timer = QtCore.QTimer()
        #timer.timeout.connect(quit_func)
        timer.timeout.connect(lambda: None)  # Let the interpreter run each 500 ms.
        timer.start(300)  # You may change this if you wish.
        
#        try:
#            while ipc_vd.running:
#                #eprint('main running')
#                time.sleep(0.05)
#                
#        except KeyboardInterrupt:
#            eprint('interrupted!')
#            ipc_vd.dataslave.quit_from_cmd()
#            #ipc_vd.dataslave.running = False
#            #ipc_vd.retmaster.running = False
#            eprint('interrupted! 2')
#            sys.exit(0)
#        eprint('AAAAAAAAAAAAAAAAAAAAAAAA bye')
        print('BBBBBBBBBBBBBBBBBBBBB', flush=True)
        sys.exit(app.exec_())
        print('CCCCCCCCCCCCCCCCCCCCCCC', flush=True)


#def AXXX(*args, **kwargs):    
#    file = QtCore.QFile()
#    if not file.open(1, QtCore.QIODevice.WriteOnly | QtCore.QIODevice.Text):
#        return
#    
#    file.write()
#    file.close()
#    print(*args, file=sys.stderr, **kwargs)
    
    
#if __name__ == '__main__':
#    eprint('AAAAAAAAAAAAAAAAAAAAAAAA')
##    sys.stdout = os.fdopen(sys.stdout.fileno(), 'wb', 0)
##    sys.stderr = os.fdopen(sys.stderr.fileno(), 'wb', 0)    
#    ipc_vd = IPCVehicleDetection()
##    #ipc_vd.start()
##
##    eprint('start vehicle_detection loop')
#    try:
#        while ipc_vd.running:
#            #eprint('main running')
#            time.sleep(50.0/1000.0)
#            
#    except KeyboardInterrupt:
#        eprint('interrupted!')
#        ipc_vd.dataslave.running = False
#        ipc_vd.retmaster.running = False
#        eprint('interrupted! 2')
#    eprint('AAAAAAAAAAAAAAAAAAAAAAAA bye')