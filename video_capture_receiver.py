import numpy as np
import cv2
import sys, os
import subprocess

import time
import datetime
import logging
log_filename = datetime.datetime.now().strftime("log/tk%Y-%m-%d_%H_%M_%S.log")
logging.basicConfig(level=logging.DEBUG,
            format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
            datefmt='%m-%d %H:%M:%S',
            filename='mylog.log')

console = logging.StreamHandler()
console.setLevel(logging.INFO)

formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

e_devicetype_hmx_3d_camera = 0
e_devicetype_etron_3d_camera = 1

data_from_stderr = 0

print(cv2.__version__)

#def sprintf(buf, fmt, *args):
#    buf.write(fmt % args)

class VideoCaptureReceiver():
    '''
    This class forks himax app process which captures image from himax device on demand.
    When calling read(), a command is sent to child process,
    it then get image from himax device then return to parent process through PIPE.
    The child process is terminated when instance of this class is destroyed.
    '''

    EXE_PATH = "bin/x64/Release/VideoCaptureTransmitter.exe"

    # For IPC with (himax) capture process
    CMD_REQ_IMG = b'\x03'
    CMD_QUIT = b'\x04'

    NIR_IMG_WIDTH = 1280
    NIR_IMG_HEIGHT = 720
    NIR_IMG_CHANNEL = 3
    NIR_IMG_DEPTH = 1
    NIR_IMG_SIZE = NIR_IMG_WIDTH * NIR_IMG_HEIGHT * NIR_IMG_CHANNEL * NIR_IMG_DEPTH
    DEP_IMG_WIDTH = 1280#216
    DEP_IMG_HEIGHT = 720#124
    DEP_IMG_CHANNEL = 1
    DEP_IMG_DEPTH = 2
    DEP_IMG_SIZE = DEP_IMG_WIDTH * DEP_IMG_HEIGHT * DEP_IMG_CHANNEL * DEP_IMG_DEPTH

    def __init__(self, deviceType):
        self.proc = None
        self.deviceType = deviceType
        self.depthCnt = 0
        self.one = 1
        self.one2 = 1
    def __del__(self):
        #logging.info('__del__')
        #print('__del__')
        self.stop()

    def open(self):
        if not self.proc:
            print('os.path.exists(VideoCaptureReceiver.EXE_PATH)=',os.path.exists(VideoCaptureReceiver.EXE_PATH))
            if os.path.exists(VideoCaptureReceiver.EXE_PATH):
                if data_from_stderr == 1:
                    self.proc = subprocess.Popen(VideoCaptureReceiver.EXE_PATH,
                                                 stdin = subprocess.PIPE,
                                                 stdout = subprocess.PIPE,
                                                 stderr = sys.stderr)                    
                else:                        
                    self.proc = subprocess.Popen(VideoCaptureReceiver.EXE_PATH,
                                                 stdin = subprocess.PIPE,
                                                 stdout = subprocess.PIPE,
                                                 stderr = sys.stderr)

    def is_opened(self):
        return self.proc != None

    def release(self):
        pass # always keep process alive, do nothing here

    def stop(self):
        if self.proc:
            self._send_cmd(VideoCaptureReceiver.CMD_QUIT)
            self.proc.wait()
            self.proc = None

    def _send_cmd(self, cmd):
        #logging.info("_send_cmd=")
        #logging.info(cmd)
        #print("_send_cmd=", cmd)
        try:
            stdin = self.proc.stdin
            stdin.write(cmd)
            stdin.flush()
            #logging.info("_send_cmd finish")
        except:
            print("Failed to send cmd to vidacp transmitter process")

    def read(self):
        '''
        Returns (ret, nir, dep)
            ret: whether query is success
            nir: nir image
            dep: depth image
        '''

        ret = False
        nir = None
        dep = None

        if self.proc:
            try:
                self._send_cmd(VideoCaptureReceiver.CMD_REQ_IMG)
                nir = self._read_nir()
                dep = self._read_dep()
                ret = True
            except:
                raise RuntimeError("Failed to read img from vidcap transmitter process")

        return (ret, nir, dep)

    def _read_nir(self):
        #logging.info('_read_nir')
        if data_from_stderr == 1:
            stdout = self.proc.stderr
        else:
            stdout = self.proc.stdout
        
        nir = stdout.read(VideoCaptureReceiver.NIR_IMG_SIZE)
        nir = np.frombuffer(nir, dtype=np.uint8, count=VideoCaptureReceiver.NIR_IMG_SIZE)
        
        if self.deviceType == e_devicetype_etron_3d_camera:
            nir.shape = (VideoCaptureReceiver.NIR_IMG_HEIGHT,
                         VideoCaptureReceiver.NIR_IMG_WIDTH,
                         VideoCaptureReceiver.NIR_IMG_CHANNEL)        
#            if self.one == 1:
#                cv2.imwrite("aa.bmp", nir)
#                self.one = 0            
        elif self.deviceType == e_devicetype_hmx_3d_camera:
            nir.shape = (VideoCaptureReceiver.NIR_IMG_WIDTH,
                         VideoCaptureReceiver.NIR_IMG_HEIGHT,
                         VideoCaptureReceiver.NIR_IMG_CHANNEL)
            nir = np.flip(nir, axis=0)
            nir = cv2.cvtColor(nir, cv2.COLOR_RGB2BGR)            
            
        return nir

    def _read_dep(self):
        if data_from_stderr == 1:
            stdout = self.proc.stderr
        else:
            stdout = self.proc.stdout     

        if self.deviceType == e_devicetype_etron_3d_camera:
                
            dep16 = stdout.read(VideoCaptureReceiver.DEP_IMG_SIZE)
            # /2 => since uint16 is 2 bytes, count is SIZE/2
            # /8 => convert depth to mm
            dep = np.frombuffer(dep16, dtype=np.uint16, count=VideoCaptureReceiver.DEP_IMG_SIZE //2) / 8  
            
            dep.shape = (VideoCaptureReceiver.DEP_IMG_HEIGHT,
                         VideoCaptureReceiver.DEP_IMG_WIDTH,
                         VideoCaptureReceiver.DEP_IMG_CHANNEL)     
            if self.depthCnt < 10:
                if self.depthCnt == 9:
                    logging.info(dep[0,0,0])
                    logging.info(dep[0,1,0])
                    logging.info(dep[0,2,0])
                    logging.info(dep[0,3,0])
                    logging.info(dep[0,4,0])
                    logging.info(dep[0,5,0])
                    logging.info(dep[0,6,0])
                    logging.info(dep[0,7,0])
                name = "depth_16_"
                ext = ".raw"
                buf = "%s%d%s" % (name, self.depthCnt, ext)

                fo = open(buf, "wb")
                fo.write(dep16)
                self.depthCnt += 1
                

                 
        elif self.deviceType == e_devicetype_hmx_3d_camera:
            dep = stdout.read(VideoCaptureReceiver.DEP_IMG_SIZE)
            # /2 => since uint16 is 2 bytes, count is SIZE/2
            # /8 => convert depth to mm
            dep = np.frombuffer(dep, dtype=np.uint16, count=VideoCaptureReceiver.DEP_IMG_SIZE //2) / 8     
            dep.shape = (VideoCaptureReceiver.DEP_IMG_HEIGHT,
                         VideoCaptureReceiver.DEP_IMG_WIDTH,
                         VideoCaptureReceiver.DEP_IMG_CHANNEL)
            dep = np.flip(dep, axis=0)

        return dep


class VideoCapture():
    DEFAULT_CAM_IDX = 0

    def __init__(self, deviceType, use_webcam = False):

        self.use_webcam = use_webcam
        self.opened = False

        if use_webcam:
            self.webcap = cv2.VideoCapture(VideoCapture.DEFAULT_CAM_IDX)
        else:
            self.remotecap = VideoCaptureReceiver(deviceType)

    def open(self):
        if self.is_opened():
            return

        if self.use_webcam:
            self.webcap.open(VideoCapture.DEFAULT_CAM_IDX)
        else:
            self.remotecap.open()

    def is_opened(self):
        if self.use_webcam:
            return self.webcap.isOpened()
        else:
            return self.remotecap.is_opened()

    def release(self):
        if not self.is_opened():
            return

        if self.use_webcam:
            self.webcap.release()
        else:
            self.remotecap.release()

    def read(self):
        '''
        Returns (result, rgb, depth)
        '''
        if self.use_webcam:
            ret, rgb = self.webcap.read()
            return (ret, rgb, None)
        else:
            return self.remotecap.read()


###################
##    Testing    ##
###################

def _show_img(img, title):
    if not img is None:
        cv2.imshow(title, img)
        #cv2.waitKey(1000)
        cv2.waitKey(33)

def _test(deviceType, use_webcam, cnt, show_img):
    capture = VideoCapture(deviceType, use_webcam)
    capture.open()
    logging.info(cnt)
    while cnt < 3000:
        ret, nir, dep = capture.read()
        if ret:
            if show_img:
                _show_img(nir, "NIR")
                _show_img(dep, "DEPTH")
        else:
            raise RuntimeError("Failed to capture image")
        cnt += 1

        #logging.info(cnt)

    cv2.destroyAllWindows()

if __name__ == "__main__":
    cnt = 0
    show_img = True
    _test(e_devicetype_etron_3d_camera, False, cnt, show_img) 
    #_test(e_devicetype_etron_3d_camera, True, cnt, show_img)  # webcam
