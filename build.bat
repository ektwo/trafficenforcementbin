rmdir build /s /q
rmdir dist  /s /q
rmdir __pycache__  /s /q
del  demo.spec /s /q
del demo.exe /s /q
pyinstaller -F --clean --onefile^
            --paths d:\winapp\venv\wpy36\Lib\site-packages\PyQt5\Qt\bin;.\dll;.\bmp^
            --add-binary .\bmp\kneron_logo.png;bmp^
            --add-binary .\bmp\kneron.ico;bmp^
            --add-binary .\bmp\unknown_person.jpg;bmp^
            --add-binary liveness_5layers.hdf5;.^
            --hidden-import="resource" --hidden-import="posix"^
             demo.py

copy .\dist\demo.exe .
echo "================================"
echo "Start Program"
REM demo.exe --gui
