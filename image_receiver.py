# -*- coding: utf-8 -*-
"""
Created on Mon Oct 22 10:17:21 2018

@author: james.chan
"""
#!/usr/bin/env python

from PyQt5.QtCore import QBuffer, QDataStream, QSharedMemory, QSystemSemaphore
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QApplication, QDialog, QFileDialog

from dialog import Ui_Dialog

USE_GUI=False


from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import numpy as np
import cv2
import os
import sys
import time
import datetime
import logging
log_filename = datetime.datetime.now().strftime("log/tk%Y-%m-%d_%H_%M_%S.log")
logging.basicConfig(level=logging.DEBUG,
            format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
            datefmt='%m-%d %H:%M:%S',
            filename='mylog.log')

console = logging.StreamHandler()
console.setLevel(logging.INFO)

formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

def QImageToCvMat(qImg):
    '''  Converts a QImage into an opencv MAT format  '''

    qImg = qImg.convertToFormat(QImage.Format_RGB888)#Format_RGB32)

    width = qImg.width()
    height = qImg.height()

    ptr = qImg.constBits()
    arr = np.array(ptr).reshape(height, width, 4)  #  Copies the data
    return arr


#def QImage2CV(qImg):
#
#    tmp = qImg
#    
#    #使用numpy创建空的图象
#    cv_image = np.zeros((tmp.height(), tmp.width(), 3), dtype=np.uint8)
#    
#    for row in range(0, tmp.height()):
#        for col in range(0,tmp.width()):
#            r = qRed(tmp.pixel(col, row))
#            g = qGreen(tmp.pixel(col, row))
#            b = qBlue(tmp.pixel(col, row))
#            cvImg[row,col,0] = r
#            cvImg[row,col,1] = g
#            cvImg[row,col,2] = b
#    
#    return cvImg
    
def CV2QImage(cvImg):
    height, width, channel = cvImg.shape
    bytesPerLine = 3 * width
    qImg = QImage(cvImg.data, width, height, bytesPerLine, QImage.Format_RGB888)
    #qImg = QImage(cvImg.data, width, height, bytesPerLine, QImage.Format_RGB888).rgbSwapped()
    return qImg


class Dialog(QDialog):
    def __init__(self, parent = None):
        super(Dialog, self).__init__(parent)

        self.sharedMemory = QSharedMemory('sharedmemory_data3')

        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        #self.ui.loadFromFileButton.clicked.connect(self.loadFromFile)
        self.ui.loadFromSharedMemoryButton.clicked.connect(self.loadFromMemory)

        self.setWindowTitle("SharedMemory Example")

        
    def loadFromMemory(self):


        if not self.sharedMemory.attach():
            self.ui.label.setText(
                    "Unable to attach to shared memory segment.\nLoad an "
                    "image first.")
            return
 
        buf = QBuffer()
        ins = QDataStream(buf)
        image = QImage()

        self.sharedMemory.lock()
        buf.setData(self.sharedMemory.constData())
        buf.open(QBuffer.ReadOnly)
        ins >> image
        self.sharedMemory.unlock()
        self.sharedMemory.detach()

        self.ui.label.setPixmap(QPixmap.fromImage(image))

    def detach(self):

        if not self.sharedMemory.detach():
            self.ui.label.setText("Unable to detach from shared memory.")

class ImageReceiver(object):
    isImageReady = False
    def __init__(self):
        self.sharedMemory = QSharedMemory('sharedmemory_data3')
        #self.sem = QSystemSemaphore("SemaphoreWriteDone", 3, QSystemSemaphore.Create)
        pass

    def __del__(self):
        self.sem.release()
        pass

    def ReceiveImage(self):
        logging.info('ReceiveImage')
        #self.sem.acquire() 
#        if not self.sharedMemory.attach():
#            logging.info("Unable to attach to shared memory segment.\nLoad an "
#                "image first.")            
#            print("Unable to attach to shared memory segment.\nLoad an "
#                "image first.")
#            return
        
        #logging.info('ReceiveImage 2')
        buf = QBuffer()
        ins = QDataStream(buf)
        image = QImage()
    
        #self.sharedMemory.lock()
        buf.setData(self.sharedMemory.constData())
        buf.open(QBuffer.ReadOnly)
        ins >> image
        #self.sharedMemory.unlock()
        #self.sharedMemory.detach()
    
        #self.ui.label.setPixmap(QPixmap.fromImage(image))
        cvImg = QImageToCvMat(image)
        #image.save(os.getcwd() + "qimg-from-cv_img.bmp")
         
        cv2.imshow("cvImg", cvImg)
        
        #self.sem.release()
        #logging.info('ReceiveImage end')
        #cv2.waitKey()
        #cv2.destroyAllWindows()

if __name__ == '__main__':

    if USE_GUI is True:
        app = QApplication(sys.argv)
        #sys.stdout = os.fdopen(sys.stdout.fileno(), 'wb', 0)
        #sys.stderr = os.fdopen(sys.stderr.fileno(), 'wb', 0)
        dialog = Dialog()
        dialog.show()
        sys.exit(app.exec_())
    else:
        logging.info('__main__ go')
        imgReceiver = ImageReceiver()
        while True:
            imgReceiver.ReceiveImage()
            logging.info('sleep 1')
            print("sleep 1")                    
            time.sleep(1)
##        while True:
##            choice = input("> ")
##        
##            if choice == 'b' :
##                logging.info('you win')
##                print("You win")
##                input("yay")
##                while True:
##                    imgReceiver.ReceiveImage()
##                    logging.info('sleep 1')
##                    print("sleep 1")                    
##                    time.sleep(1)
##                break
##        try:
##            while 1:
##                try:
##                    c = sys.stdin.read(1)
##                    print('Got character", repr(c))
##                except IOError: pass
##        finally:
##            termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
##            fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)
##        
##        count = 0
##        while (count < 9):
##           logging.info('The count is:', count)
##           imgReceiver.ReceiveImage()
##           count = count + 1
#         
#        logging.info('Good bye!')
#        
#    
#while True:
#    logging.info("test")
#    print('test')
#    sys.stdout.flush()
#    time.sleep(2)